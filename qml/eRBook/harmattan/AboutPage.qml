// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "Component"
import "Delegate"
import "ListComp"
import "../common"
import "../scripts/UIConstants.js" as Const


MyPage{
    id: root

    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-back"
            onClicked: {
                root.pageStack.pop()
            }
        }
    }

    headerText: qsTr("About")


    Image{
        id: devpdaLogo
        z: -1000
        anchors {right: parent.right; bottom: parent.bottom; margins: Const.PADDING_LARGE}
        smooth: true
        source: "../images/devpda.png"
        width: sourceSize.width
        height: sourceSize.height
        sourceSize.width: window.inPortrait ? parent.width/2 : parent.height/2
        sourceSize.height: window.inPortrait ? parent.width/2 : parent.height/2
        opacity: 0.6
    }


    Flickable{
        anchors {fill: parent; topMargin: headerMargin; margins: Const.PADDING_LARGE}
        flickableDirection: Flickable.VerticalFlick
        contentHeight: content.height

        Column{
            id: content
            anchors {left: parent.left; right: parent.right}
            spacing: 5


            Label{
                text: qsTr("eRBook")
                font.pixelSize: Const.FONT_XLARGE*2
                width: parent.width
            }
            Label{
                text: qsTr("version %1").arg(appVersion)
                font.pixelSize: Const.FONT_LSMALL
                width: parent.width
            }
            Label{
                text: qsTr("Copyright (c) 2010-2013 DevPDA<br/><a  href='http://devpda.net'>www.devpda.net</a>")
                font.pixelSize: Const.FONT_LSMALL
                width: parent.width
            }

            Separator{}


            Label{
                text: qsTr( "A fully featured Readability client for Symbian and MeeGo Smartphones that allows you to take your reading list." );
                font.pixelSize: Const.FONT_LARGE
                width: parent.width
            }

            Separator {}

            Label{
                text: qsTr("Privacy policy")
                font.pixelSize: Const.FONT_XLARGE
                onLinkActivated: privacyDialog.open()
            }

            Label{
                text: qsTr("eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook.")
                font.pixelSize: Const.FONT_SLARGE
                font.italic: true
                width: parent.width
            }
        }
    }

}
