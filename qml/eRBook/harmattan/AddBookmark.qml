import QtQuick 1.1
import com.nokia.meego 1.1

import "Component"
import "Delegate"
import "../common"
import "../scripts/UIConstants.js" as Const


MyPage{
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-back"
            onClicked: root.pageStack.pop()
        }
    }


    onStatusChanged: {
        if (status == PageStatus.Active){

        }
    }



    headerText: qsTr("Add article")


    Connections{
        target: api
        onErrorAddBookmark: {
            workingShow = false
        }

        onAddBookmarkDone: {
            api.bookmarks()
        }
        onBookmarksDone:{
            workingShow = false
            root.pageStack.pop()
        }
    }


    Flickable{
        id: flicker
        enabled: !workingShow
        opacity: workingShow ? 0.2 : 1
        anchors {fill: parent; topMargin: headerMargin; leftMargin: Const.PADDING_MEDIUM; rightMargin: Const.PADDING_LARGE}

        flickableDirection: Flickable.VerticalFlick
        contentHeight: articleUrlTitle.height +
                       urlEdit.height +
                       urlPaste.height +
                       separe.height +
                       favoriteLabel.height +
                       favRadio.height +
                       separe2.height +
                       archiveLabel.height +
                       archRadio.height +
                       separe3.height +
                       addArticle.height +
                       (Const.PADDING_MEDIUM*11)

        clip: true


        Behavior on opacity {PropertyAnimation{duration: workingShow ? 150 : 200}}


        Label{
            id: articleUrlTitle
            anchors {top: parent.top; left: parent.left; right: parent.right; topMargin: Const.PADDING_MEDIUM}
            font.pixelSize: Const.FONT_LARGE
            text:  qsTr("Article Url");
        }

        TextField{
            id: urlEdit
            anchors {left: parent.left; right: parent.right; top: articleUrlTitle.bottom; topMargin: Const.PADDING_MEDIUM}
            placeholderText:  qsTr("required")
            width: parent.width
            inputMethodHints: Qt.ImhNoAutoUppercase | Qt.ImhPreferLowercase | Qt.ImhLowercaseOnly
        }

        Button{
            id: urlPaste
            anchors {right: parent.right; top: urlEdit.bottom; topMargin: Const.PADDING_MEDIUM}
            text: {
                if (urlEdit.text === ""){
                    return qsTr("Paste Url")
                }else{
                    return qsTr("Clear")
                }
            }
            onClicked: {
                if (urlEdit.text === ""){
                    urlEdit.text = helper.getText()
                }else{
                    urlEdit.text = ""
                }
            }
        }

        Separator{
            id: separe
            anchors {top: urlPaste.bottom; topMargin: Const.PADDING_MEDIUM}
        }


        Label{
            id: favoriteLabel
            anchors {left: parent.left; right: parent.right; top: separe.bottom; topMargin: Const.PADDING_MEDIUM}
            font.pixelSize: Const.FONT_LARGE
            text:  qsTr("Favorite");
        }


        Switch{
            id: favRadio
            anchors {right: parent.right; top: favoriteLabel.bottom; topMargin: Const.PADDING_MEDIUM}
        }


        Image{
            id: favoriteImage
            anchors {left: parent.left; verticalCenter: favRadio.verticalCenter}
            source: !theme.inverted ? favRadio.checked ? "../images/favourite_inverted_yes.svg" :
                                                 "../images/favourite_inverted_no.png" :
                                      favRadio.checked ? "../images/favourite_yes.svg" :
                                                 "../images/favourite_no.png"
        }


        Separator{
            id: separe2
            anchors {top: favRadio.bottom; topMargin: Const.PADDING_MEDIUM}
        }


        Label{
            id: archiveLabel
            anchors {left: parent.left; top: separe2.bottom; topMargin: Const.PADDING_MEDIUM}
            font.pixelSize: Const.FONT_LARGE
            text:  qsTr("Archive");
        }


        Switch{
            id: archRadio
            anchors {right: parent.right; verticalCenter: archiveLabel.verticalCenter}
        }


        Separator{
            id: separe3
            anchors {top: archRadio.bottom; topMargin: Const.PADDING_MEDIUM}
        }


        Button{
            id: addArticle
            anchors {horizontalCenter: parent.horizontalCenter; top: separe3.bottom; topMargin: Const.PADDING_MEDIUM}
            text:  qsTr("Add Article")
            width: window.inPortrait ? parent.width/2 : parent.height/2
            onClicked: {
                if( urlEdit.text.length > 0){
                    workingText = qsTr("Adding article")
                    workingShow = true
                    api.addBookmark(urlEdit.text, (favRadio.checked ? 1 : 0), (archRadio.checked ? 1 : 0))
                }else{
                    console.debug("REQUIRED FIELD")
                }
            }
        }
    }
}
