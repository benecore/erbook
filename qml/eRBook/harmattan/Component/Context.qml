// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1


import "../../scripts/createobject.js" as Dynamic

ContextMenu{
    id: root




    property bool favorited
    property bool readed

    signal favoriteClicked
    signal readedClicked
    signal deleteClicked


    MenuLayout {
        MenuItem {
            text: bModel.cFavorite ? qsTr("Remove from favorite") : qsTr("Add to favorite")
            onClicked: root.favoriteClicked()
        }
        MenuItem {
            text: bModel.cArchive ? qsTr("Set as unread") : qsTr("Set as read")
            onClicked: root.readedClicked()
        }
        MenuItem {
            text: qsTr("Remove")
            onClicked: root.deleteClicked()
        }
    }
}
