// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1

import "../../scripts/UIConstants.js" as Const

Item{
    id: root
    width: root.ListView.view.width

    height: titleS.height + excerpt.height + (Const.PADDING_MEDIUM*2) + domainLabel.height


    signal clicked
    signal pressAndHold

    opacity: mouseArea.pressed ? 1 : archive ? 0.6 : 1

    Rectangle{
        anchors {left: parent.left; right: parent.right; top: parent.bottom}
        height: 1
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
    }


    Rectangle{
        visible: mouseArea.pressed && !theme.inverted
        anchors {left: parent.left; right: parent.right; top: parent.top; bottom: parent.bottom}
        color: "#860000"
        smooth: true
    }

    BorderImage {
        id: background
        anchors.fill: parent
        visible: mouseArea.pressed && theme.inverted
        source: theme.inverted ?
                    'image://theme/meegotouch-list-fullwidth-inverted-background-pressed-vertical-center' :
                    'image://theme/meegotouch-list-fullwidth-background-pressed-vertical-center'
    }


    Image {
        id: favoriteImage
        anchors {right: parent.right; top: parent.top; margins: Const.PADDING_MEDIUM}
        visible: favorite
        source: !theme.inverted ? favorite ? "../../images/favourite_inverted_yes.svg" :
                                             "../../images/favourite_inverted_no.png" :
        favorite ? "../../images/favourite_yes.svg" :
            "../../images/favourite_no.png"
        smooth: true
    }


    Label{
        id: domainLabel
        anchors {left: parent.left; top: parent.top; right: parent.right; margins: Const.PADDING_MEDIUM}
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
        font.pixelSize: Const.FONT_SMALL
        text: domain

    }


    Label{
        id: titleS
        anchors {left: parent.left; top: domainLabel.bottom; right: favoriteImage.left; leftMargin: Const.PADDING_MEDIUM;
            rightMargin: Const.PADDING_MEDIUM}
        verticalAlignment: Text.AlignTop
        maximumLineCount: 2
        wrapMode: Text.WordWrap
        elide: Text.ElideRight
        text: title
        font.pixelSize: Const.FONT_LARGE
        color: !theme.inverted ? mouseArea.pressed ? "white" : "black" : mouseArea.pressed ? "white" : "white"
    }


    Label{
        id: excerpt
        anchors {left: parent.left; top: titleS.bottom; right: parent.right; margins: Const.PADDING_MEDIUM}
        elide: Text.ElideRight
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
        font.pixelSize: Const.FONT_SMALL
        text: date_added
    }


    Label{
        id: archiveLabel
        visible: archive
        anchors {right: parent.right; top: titleS.bottom; margins: Const.PADDING_MEDIUM}
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
        font.pixelSize: Const.FONT_SMALL
        text: qsTr("readed")
    }


    MouseArea{
        id: mouseArea
        anchors {fill: parent}
        onClicked: {
            root.clicked()
        }
        onPressAndHold: root.pressAndHold()
        onPressed: {
            list.currentIndex = index
            bModel.setCData(list.currentIndex)
            console.debug("CURRENT TITLE:\n"+bModel.cTitle)
        }
    }
}
