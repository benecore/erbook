import QtQuick 1.1

import "../../scripts/UIConstants.js" as Const

Item{
    id: root
    height: 0
    width: ListView.view.width

    Item{
        id: container
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.top
        anchors.bottomMargin: Const.PADDING_XXLARGE
        height: pullIcon.height
        width: pullIcon.width + textColumn.width + textColumn.anchors.leftMargin
        visible: root.ListView.view.__wasAtYBeginning && root.ListView.view.__initialContentY - root.ListView.view.contentY > 10

        Image{
            id: pullIcon
            anchors.left: parent.left
            source: !theme.inverted ? "image://theme/icon-m-toolbar-next" : "image://theme/icon-m-toolbar-next-white-selected"
            sourceSize.width: Const.SIZE_ICON_DEFAULT
            sourceSize.height: Const.SIZE_ICON_DEFAULT
            rotation: visible && root.ListView.view.__initialContentY - root.ListView.view.contentY > 100 ? 270 : 90

            Behavior on rotation { NumberAnimation{ duration: 250 } }
        }

        Column{
            id: textColumn
            width: Math.max(pullText.width)
            height: childrenRect.height
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: pullIcon.right
            anchors.leftMargin: Const.PADDING_LARGE

            Text{
                id: pullText
                font.pixelSize: Const.FONT_SLARGE
                color: !theme.inverted ? Const.COLOR_FOREGROUND : Const.COLOR_INVERTED_FOREGROUND
                text: visible && root.ListView.view.__initialContentY - root.ListView.view.contentY > 100 ?
                          qsTr("Release to sync") : qsTr("Pull down to sync")
            }
        }
    }
}
