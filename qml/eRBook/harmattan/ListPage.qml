// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1


import "Component"
import "Delegate"
import "ListComp"
import "../common"
import "../scripts/UIConstants.js" as Const
import "../scripts/createobject.js" as Dynamic
import "../scripts/readability.js" as Readability

MyPage{
    id: root
    tools: ToolBarLayout{
        ToolIcon{
            platformIconId: "toolbar-add"
            onClicked: {
                root.pageStack.push(Qt.resolvedUrl("AddBookmark.qml"))
            }
        }
        ToolIcon{
            platformIconId: "toolbar-list"
            onClicked: {
                filterDialog.open()
            }
        }
        ToolIcon{
            platformIconId: "toolbar-settings"
            onClicked: {
                root.pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
            }
        }
        ToolIcon{
            platformIconId: "toolbar-view-menu"
            onClicked: {
                if (menu.status === DialogStatus.Open)  { menu.close() } else { menu.open() }
            }
        }
    }


    headerText: qsTr("Articles")
    onStatusChanged: {
        if (status === PageStatus.Active && !bModel.count){
            if (db.sync){
                api.bookmarks()
            }else{
                api.offlineBookmarks()
            }
        }
    }


    Menu{
        id: menu
        MenuLayout{
            MenuItem{
                text: qsTr("Sync articles")
                onClicked: {
                    workingText = qsTr("Syncing")
                    workingShow = true
                    api.bookmarks()
                }
            }
            MenuItem{
                text: qsTr("About")
                onClicked: {
                    menu.close()
                    root.pageStack.push(Qt.resolvedUrl("AboutPage.qml"))
                }
            }
        }
    }


    Connections{
        target: bModel
        onCountChanged: console.debug("COUNT: "+bModel.count)
    }


    Connections{
        target: api
        onCurrentSyncing: {
            if (api.syncing){
                workingText = qsTr("Syncing %1/%2").arg(actualItem).arg(allItem)
            }
        }
    }


    FilteringDialog{
        id: filterDialog
    }

    DeleteDialog{
        id: deleteQuery
        onAccepted: {
            workingText = qsTr("Removing article")
            workingShow = true
            api.deleteBookmark()
        }
    }


    Context{
        id: contextMenu
        onReadedClicked: {
            workingText = bModel.cArchive ? qsTr("Set as unread") : qsTr("Set as read")
            workingShow = true
            api.updateBookmark(bModel.cFavorite, bModel.cArchive ? 0 : 1)
        }
        onDeleteClicked: {
            deleteQuery.open()
        }
        onFavoriteClicked: {
            workingText = bModel.cFavorite ? qsTr("Remove from favorite") : qsTr("Add to favorite")
            workingShow = true
            api.updateBookmark(bModel.cFavorite ? 0 : 1, bModel.cArchive)
        }
    }



    Label{
        id: noResult
        opacity: bModel.count || workingShow || api.syncing ? 0 : 1
        anchors {horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: parent.height/2}
        color: Const.COLOR_INVERTED_SECONDARY_FOREGROUND
        font.pixelSize: 45
        text: {
            if (db.viewMode === "all"){
                return qsTr("No articles")
            }
            else if (db.viewMode === "archive"){
                return qsTr("No read articles")
            }
            else if (db.viewMode === "non_archive"){
                return qsTr("No unread articles")
            }
        }

        Behavior on opacity {PropertyAnimation{duration: 200}}
    }



    AbstractListView{
        id: list
        anchors {fill: parent; topMargin: headerMargin}
        opacity: workingShow || api.syncing || !bModel.count ? 0.0 : 1
        cacheBuffer: 150
        model: bModel
        delegate: ListDelegate{
            onClicked: root.pageStack.push(Qt.resolvedUrl("ReadPage.qml"), {workingText: qsTr("Loading content"), workingShow: true})
            onPressAndHold: contextMenu.open()
        }


        onPullDownRefresh: {
            workingText = qsTr("Syncing")
            workingShow = true
            api.bookmarks()
        }

        Behavior on opacity {PropertyAnimation{duration: workingShow ? 100 : 500}}
    }


    ScrollDecorator{
        flickableItem: list
    }
}
