// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1


import "Component"
import "../common"
import "../scripts/UIConstants.js" as Const
import "../scripts/readability.js" as Readability

MyPage{
    id: root
    headerText: qsTr("Sign In")


    //orientationLock: PageOrientation.LockPortrait


    SecondBusy{
        id: secondBusy
        anchors {centerIn: parent}
        show: workingShow
        textLabel: qsTr("Please wait")
    }


    Connections{
        target: api
        onRequestTokenDone: {
            workingShow = false
            window.pageStack.push(Qt.resolvedUrl("AuthPage.qml"), {webUrl: authUrl})
        }
    }


    Flickable{
        anchors {fill: parent; topMargin: headerMargin+Const.MARGIN_XLARGE; margins: Const.MARGIN_XLARGE}
        flickableDirection: Flickable.VerticalFlick
        contentHeight: content.height
        opacity: workingShow ? 0.3 : 1


        Behavior on opacity { PropertyAnimation {duration: 200}}

        Column{
            id: content
            anchors {left: parent.left; right: parent.right}
            spacing: 10


            Image{
                anchors {horizontalCenter: parent.horizontalCenter}
                smooth: true
                source: "../images/logo.png"
                width: sourceSize.width
                height: sourceSize.height
                sourceSize.width: 100
                sourceSize.height: 100
            }


            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                text: qsTr("eRBook %1").arg(appVersion)
                font.pixelSize: Const.FONT_XLARGE
            }

            Label{
                anchors {horizontalCenter: parent.horizontalCenter}
                width: parent.width
                wrapMode: Text.WordWrap
                text: qsTr("need access to you Readability account. Just click on Sign In button.")
                font.italic: true
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter
                Button{
                    id: signIn
                    text: qsTr("Sign In")
                    width: parent.width - signUp.width
                    onClicked: {
                        workingShow = true
                        Readability.getRequestToken()
                        //api.requestToken()
                    }
                }
                Button{
                    id: signUp
                    width: parent.width/2
                    text: qsTr("Sign Up")
                    onClicked: Qt.openUrlExternally("https://www.readability.com/readers/register")
                }
            }

            Separator{}

            Label{
                id: privacy
                anchors {horizontalCenter: parent.horizontalCenter;}
                text: qsTr("Privacy policy")
                font.pixelSize: Const.FONT_SLARGE
                font.bold: true
                color: "red"
            }
            Label{
                id: privacyText
                anchors {horizontalCenter: parent.horizontalCenter}
                width: parent.width
                wrapMode: Text.WordWrap
                font.italic: true
                text: qsTr("eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook.")
            }
        }
    }
}
