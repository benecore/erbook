// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.meego 1.1
import com.nokia.extras 1.1

import "../common"
import "Component"
import "../scripts/UIConstants.js" as Const
import "../scripts/createobject.js" as Dynamic

PageStackWindow{
    id: window
    showStatusBar: inPortrait
    showToolBar: true
    initialPage: db.authorized ? listPage : Qt.resolvedUrl("LoginPage.qml")




    CornerImage{
        parent: window.pageStack
    }


    InfoBanner{
        id: banner
        topMargin: Const.MARGIN_XLARGE
    }


    ListPage{
        id: listPage
        workingText: qsTr("Syncing")
        workingShow: true
    }


    ErrorDialog{
        id: errorDialog
    }


    Connections{
        target: db
        onSortByChanged: bModel.sortingBy = sortBy
        onSortOrderChanged: bModel.sortingOrder = sortOrder
        onViewModeChanged: bModel.filterType = viewMode
    }

    Connections{
        target: api
        onError: {
            if (db.debug){
                var dialog = Dynamic.createObject(Qt.resolvedUrl("Component/ErrorDialog.qml"), window.pageStack)
                dialog.errorCode = code
                dialog.errorText = message
                dialog.open()
            }
            else{
                console.debug("DEBUGGING DISABLED")
            }
        }

        onBanner: {
            banner.text = message
            banner.show()
        }
        onUpdateBookmarkDone: {
            var json = JSON.parse(result)
            bModel.cFavorite = json.favorite
            bModel.cArchive = json.archive
            db.setArchiveCache(bModel.cArticleId, json.archive)
            db.setFavoriteCache(bModel.cArticleId, json.favorite)
            if (window.pageStack.currentPage == listPage){
                bModel.reSort()
            }
            window.pageStack.currentPage.workingShow = false
        }
        onDeleteBookmarkDone: {
            db.deleteFromCacher(bModel.cArticleId)
            bModel.remove()
            window.pageStack.currentPage.workingShow = false
        }
        onBookmarksDone: {
            bModel.reSort()
            window.pageStack.currentPage.workingShow = false
        }
        onShortUrlDone: {
            window.pageStack.currentPage.workingShow = false
            db.setShortUrl(bModel.cArticleId, url)
            shareUI.share(bModel.cTitle, url)
        }
        onShortUrlError: shareUI.share(bModel.cTitle, bModel.cUrl)
    }



    Component.onCompleted: {
        theme.inverted = db.theme
        bModel.filterType = db.viewMode
        bModel.sortingBy = db.sortBy
        bModel.sortingOrder = db.sortOrder
        //console.debug("SORTBY: "+bModel.sortingBy+"\nSORTORDER: "+bModel.sortingOrder + "\nVIEW MODE:" +bModel.filterType)
        //console.debug("\nTOKEN: "+db.getToken() + "\nSECRET: "+db.getSecret())
        bModel.reSort()
    }

}
