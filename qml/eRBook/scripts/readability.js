Qt.include("oauth.js")

var TOKEN
var SECRET
var PIN


function urlChanged(url){
    console.log(url)
    if (url.toString().indexOf("oauth_verifier") !== -1){
        var response = url.toString().split("&")
        console.debug(response)
        PIN = response[1].split("=")[1]
        console.log("oauth_verifier: "+PIN)
        //api.accessToken(PIN)
        getAccessToken(PIN)
    }
}



function getRequestToken(){
    var xhr = createOAuthHeader("POST", "https://www.readability.com/api/rest/v1/oauth/request_token/");
    xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    var response = xhr.responseText.split('&');
                    TOKEN = response[1].split('=')[1];
                    SECRET = response[0].split('=')[1];
                    auth.oauthToken = TOKEN
                    auth.oauthTokenSecret = SECRET
                    console.debug(xhr.responseText+"\nTOKEN: "+TOKEN+"\nSECRET: "+SECRET)
                    var URL = "https://www.readability.com/api/rest/v1/oauth/authorize?oauth_token="+TOKEN+"&oauth_callback=localhost";
                    window.pageStack.currentPage.workingShow = false
                    window.pageStack.replace(Qt.resolvedUrl("AuthPage.qml"), {webUrl: URL})
                    //tokenData.accessToken = response[1].split('=')[1];
                    //tokenData.secretToken = response[2].split('=')[1];
                    //loginPage.token = response[1].split('=')[1];
                    //loginPage.secret = response[2].split('=')[1];
                    //console.log(loginPage.token + "\n"+ loginPage.secret)
                    //webUrl = "https://www.readability.com/api/rest/v1/oauth/authorize?oauth_token="+loginPage.token+"&oauth_callback=http://www.devpda.net/oauth.php";
                    //console.log(loginPage.web)
                    //window.pageStack.push(loginPage)
                }
            }
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("Accept-Language", "en");
    xhr.setRequestHeader("User-Agent", "Mozilla/5.0")
    xhr.send();
}



function getAccessToken(pin){
    //console.debug("TOKEN: "+auth.oauthToken+"\nSECRET: "+auth.oauthTokenSecret)
    var xhr = createOAuthHeader("GET", "https://www.readability.com/api/rest/v1/oauth/access_token?oauth_verifier="+pin, null, {"token":auth.oauthToken,"secret":auth.oauthTokenSecret});
    xhr.onreadystatechange = function() {
                console.debug(xhr.responseText+"\n"+xhr.statusText)
                if (xhr.readyState === XMLHttpRequest.DONE) {
                    console.debug(xhr.responseText)
                    var response = xhr.responseText.split('&');
                    //tokenData.accessToken = response[0].split('=')[1];
                    //tokenData.secretToken = response[1].split('=')[1];
                    //tokenData.authorized = 1;
                    //console.log(tokenData.accessToken + "\n" + tokenData.secretToken + "\n" + tokenData.authorized)
                    //window.pageStack.replace(mainPage)
                }
            }
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhr.setRequestHeader("User-Agent", "Mozilla/5.0")
    xhr.send();
}
