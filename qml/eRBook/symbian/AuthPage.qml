// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1

import "Component"
import "../scripts/UIConstants.js" as Const
import "../scripts/readability.js" as Readability


MyPage{
    id: root

    function checkUrlForToken() {
            if (/oauth_verifier/i.test(webView.url)) {
                //window.pageStack.replace(Qt.resolvedUrl("ListPage.qml"), {loading: true})
                api.accessToken(webView.url)
                //Readability.getAccessToken(pin)
            }
            else{
                console.debug("URL: "+webView.url)
                //root.reject();
                // messageDialog.showInfo(qsTr("You have denied access to your YouTube account"));
            }
        }


    tools: ToolBarLayout{
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-back"
            onClicked: window.pageStack.replace(Qt.resolvedUrl("LoginPage.qml"))
        }
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-refresh"
            onClicked: webView.url = webUrl
        }
    }

    property url webUrl
    onWebUrlChanged: webView.url = webUrl

    headerText: qsTr("Sign In")


    onStatusChanged: {
        if (status === PageStatus.Active){
            webView.urlChanged.connect(checkUrlForToken)
        }
    }


    Connections{
        target: api
        onAccessTokenDone: {
            window.pageStack.replace(Qt.resolvedUrl("ListPage.qml"), {loading: true})
        }
    }


    FlickableWeb{
        id: webView
        opacity: webView.progress!==1.0 ? 0 : 1
        anchors {
            top: parent.top
            topMargin: headerMargin
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        clip: true
        onUrlChanged: {
            console.debug(url)
        }

        Behavior on opacity { PropertyAnimation {duration: pro.visible ? 100 : 400}}
    }

    ProgressBar {
        id: pro
        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }
        value: webView.progress
        visible: webView.progress!==1.0
    }


    Component.onDestruction: {
        webUrl = ""
        webView.urlChanged.disconnect(checkUrlForToken)
    }
}
