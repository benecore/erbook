// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1


QueryDialog{
    id: queryDialog

    platformInverted: window.platformInverted

    acceptButtonText: qsTr("Delete")
    rejectButtonText: qsTr("Cancel")


    titleText: bModel.cTitle
    message: qsTr("Are you sure you want to delete this article")

    onRejected: reject()
}
