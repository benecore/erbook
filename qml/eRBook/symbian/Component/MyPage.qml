// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1


Page{
    id: root


    property bool loading: false
    property alias headerShow: header.visible
    property alias headerText: titleHeader.text
    property alias headerHeight: header.height
    property alias headerColor: header.color
    property int headerMargin: headerShow ? header.height : 0
    property bool isFavorite: false
    property bool isArchive: false


    property alias workingText: working.textLabel
    property bool workingShow: false



    Rectangle{
        id: header
        width: parent.width
        height: 50
        z: 1000
        smooth: true
        gradient: Gradient{
            GradientStop {position: 0.4; color: window.platformInverted ? "#980000" : "#454444"}
            GradientStop {position: 0.9; color: window.platformInverted ? "#860000" : "#3b3b3b"}
        }



        Label{
            id: titleHeader
            anchors {left: parent.left; right: parent.right; verticalCenter: parent.verticalCenter; margins: platformStyle.paddingMedium}
            elide: Text.ElideLeft
            color: "white"
            font.pixelSize: platformStyle.fontSizeLarge
            font.bold: true

        }
    }

    SecondBusy{
        id: working
        anchors {centerIn: parent}
        show: workingShow
    }

    BusyIndicator {
        id: busy
        anchors {horizontalCenter: parent.horizontalCenter; top: parent.top; topMargin: parent.height/2}
        running: visible
        visible: loading
        width: 70
        height: 70
    }
}
