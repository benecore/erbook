import QtQuick 1.1
import com.nokia.symbian 1.1



QueryDialog {
    id: dialog


    property alias text: textPrivacy.text
    property alias title: dialog.titleText

    platformInverted: window.platformInverted

    onClickedOutside: reject()
    height: contentItem.height
    content: Item {
        id: contentItem

        height: column.height+90
        anchors { top: parent.top; left: parent.left; right: parent.right; margins: 5 }

        Column {
            id: column

            anchors { top: parent.top; left: parent.left; right: parent.right }
            spacing: 20

            Label {
                id: textPrivacy
                width: parent.width
                platformInverted: window.platformInverted
                wrapMode: Text.WordWrap
            }
        }
    }
}
