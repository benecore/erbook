// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1


Item{
    id: root
    width: root.ListView.view.width

    height: titleS.height + excerpt.height + (platformStyle.paddingMedium*2) + domainLabel.height


    signal clicked
    signal pressAndHold

    opacity: mouseArea.pressed ? 1 : archive ? 0.5 : 1

    Rectangle{
        anchors {left: parent.left; right: parent.right; top: parent.bottom}
        height: 1
        color: platformStyle.colorNormalMid
    }


    Rectangle{
        visible: mouseArea.pressed
        anchors {left: parent.left; right: parent.right; top: parent.top; bottom: parent.bottom}
        color: window.platformInverted ? "#860000" : "#c9c8c8"
        smooth: true
    }


    Image {
        id: favoriteImage
        anchors {right: parent.right; top: parent.top; margins: platformStyle.paddingMedium}
        visible: favorite
        source: window.platformInverted ? favorite ? "../../images/favourite_inverted_yes.svg" :
                                             "../../images/favourite_inverted_no.png" :
                                  favorite ? "../../images/favourite_yes.svg" :
                                             "../../images/favourite_no.png"
        smooth: true
    }


    Label{
        id: domainLabel
        anchors {left: parent.left; top: parent.top; right: parent.right; margins: platformStyle.paddingMedium}
        color: platformStyle.colorNormalMid
        font.pixelSize: platformStyle.fontSizeSmall
        text: domain

    }


    Label{
        id: titleS
        anchors {left: parent.left; top: domainLabel.bottom; right: favoriteImage.left; leftMargin: platformStyle.paddingMedium;
        rightMargin: platformStyle.paddingMedium}
        verticalAlignment: Text.AlignTop
        maximumLineCount: 2
        wrapMode: Text.WordWrap
        elide: Text.ElideRight
        text: title
        font.pixelSize: platformStyle.fontSizeLarge
        color: window.platformInverted ? mouseArea.pressed ? "white" : "black" : mouseArea.pressed ? "black" : "white"
    }


    Label{
        id: excerpt
        anchors {left: parent.left; top: titleS.bottom; right: parent.right; margins: platformStyle.paddingMedium}
        elide: Text.ElideRight
        color: platformStyle.colorNormalMid
        font.pixelSize: platformStyle.fontSizeSmall
        text: date_added
    }


    Label{
        id: archiveLabel
        visible: archive
        anchors {right: parent.right; top: titleS.bottom; margins: platformStyle.paddingMedium}
        color: platformStyle.colorNormalMid
        font.pixelSize: platformStyle.fontSizeSmall
        text: qsTr("readed")
    }


    MouseArea{
        id: mouseArea
        anchors {fill: parent}
        onClicked: {
            root.clicked()
        }
        onPressAndHold: root.pressAndHold()
        onPressed: {
            privateStyle.play(0)
            list.currentIndex = index
            bModel.setCData(list.currentIndex)
            console.debug("CURRENT TITLE:\n"+bModel.cTitle)
        }
    }
}
