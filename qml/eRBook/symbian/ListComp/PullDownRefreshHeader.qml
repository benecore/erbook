import QtQuick 1.1

Item{
    id: root
    height: 0
    width: ListView.view.width

    Item{
        id: container
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.bottom: parent.top
        anchors.bottomMargin: platformStyle.paddingLarge
        height: pullIcon.height
        width: pullIcon.width + textColumn.width + textColumn.anchors.leftMargin
        visible: root.ListView.view.__wasAtYBeginning && root.ListView.view.__initialContentY - root.ListView.view.contentY > 10

        Image{
            id: pullIcon
            anchors.left: parent.left
            source: window.platformInverted ? "image://theme/toolbar-next_inverse" : "image://theme/toolbar-next"
            sourceSize.width: platformStyle.graphicSizeSmall
            sourceSize.height: platformStyle.graphicSizeSmall
            rotation: visible && root.ListView.view.__initialContentY - root.ListView.view.contentY > 100 ? 270 : 90

            Behavior on rotation { NumberAnimation{ duration: 250 } }
        }

        Column{
            id: textColumn
            width: Math.max(pullText.width)
            height: childrenRect.height
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: pullIcon.right
            anchors.leftMargin: platformStyle.paddingLarge

            Text{
                id: pullText
                font.pixelSize: platformStyle.fontSizeMedium
                color: window.platformInverted ? platformStyle.colorNormalLightInverted : platformStyle.colorNormalLight
                text: visible && root.ListView.view.__initialContentY - root.ListView.view.contentY > 100 ?
                          qsTr("Release to sync") : qsTr("Pull down to sync")
            }
        }
    }
}
