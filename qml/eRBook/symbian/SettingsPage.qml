// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1
import com.nokia.extras 1.1

import "Component"
import "Delegate"
import "../common"


MyPage{
    id: root

    tools: ToolBarLayout{
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-back"
            onClicked: root.pageStack.pop()
        }
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-menu"
            onClicked: if (menu.status === DialogStatus.Open) {menu.close()} else {menu.open()}
        }
    }

    headerText: qsTr("Settings")
    property bool parseDone: false

    onStatusChanged: {
        if (status == PageStatus.Activating){
            if (db.font === platformStyle.fontSizeSmall){
                fontSizeDialog.selectedIndex = 0
                fontSize.subTitle = fontSizeDialog.model.get(0).name;
            }
            else if (db.font === platformStyle.fontSizeMedium+2){
                fontSizeDialog.selectedIndex = 1
                fontSize.subTitle = fontSizeDialog.model.get(1).name;
            }
            else{
                fontSizeDialog.selectedIndex = 2
                fontSize.subTitle = fontSizeDialog.model.get(2).name;
            }
            if (bModel.sortingBy === "title"){
                orderDialog.selectedIndex = 0
                orderSettings.subTitle = orderDialog.model.get(0).name;
            }
            else if (bModel.sortingBy === "favorite"){
                orderDialog.selectedIndex = 1
                orderSettings.subTitle = orderDialog.model.get(1).name;
            }
            else{
                orderDialog.selectedIndex = 2
                orderSettings.subTitle = orderDialog.model.get(2).name;
            }

            if (bModel.sortingOrder === "ascending"){
                sortOrderDialog.selectedIndex = 0
                sortOrderSettings.subTitle = sortOrderDialog.model.get(0).name;
            }
            else{
                sortOrderDialog.selectedIndex = 1
                sortOrderSettings.subTitle = sortOrderDialog.model.get(1).name;
            }
            api.userInfo();
        }
    }



    Menu{
        id: menu
        platformInverted: window.platformInverted
        MenuLayout{
            MenuItem{
                enabled: !db.dbEmpty
                platformInverted: window.platformInverted
                text: qsTr("Refresh cache")
                onClicked: db.restoreDB()
            }
        }
    }



    InfoBanner{
        id: banner
        platformInverted: window.platformInverted
    }



    Connections{
        target: api
        onUserInfoDone: {
            try{
                var json = JSON.parse(result)
                username.text = json.username
            }
            catch(err){
                console.debug("UNABLE PARSE")
            }
            parseDone = true
        }
    }

    Connections{
        target: db
        onAuthRestored: {
            bModel.clear()
            root.pageStack.replace(Qt.resolvedUrl("LoginPage.qml"))
        }
        onDbRestored: {
            bModel.clear()
            banner.text = qsTr("Cache restored")
            banner.open()
        }
    }



    Flickable{
        id: flick
        width: parent.width - (platformStyle.paddingMedium*2)
        anchors {horizontalCenter: parent.horizontalCenter; top: parent.top; bottom: parent.bottom; margins: platformStyle.paddingMedium;
            topMargin: headerMargin}
        contentHeight: optionsCol.height
        clip: true;


        Column{
            id: optionsCol
            spacing : 8
            anchors {left: parent.left; right: parent.right;}
            width: parent.width


            Label{
                platformInverted: window.platformInverted
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: platformStyle.fontSizeLarge*1.2
                text: qsTr("User info");
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter;

                Label{
                    platformInverted: window.platformInverted
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Username")
                    font.pixelSize: platformStyle.fontSizeLarge
                    width: parent.width - username.width-5
                }

                Label{
                    id: username
                    opacity: parseDone ? 1 : 0
                    platformInverted: window.platformInverted
                    anchors.verticalCenter: parent.verticalCenter
                    elide: Text.ElideMiddle
                    font.pixelSize: platformStyle.fontSizeLarge

                    Behavior on opacity {PropertyAnimation{duration: 250}}
                }
            }

            Button {
                id: logOut
                //: LogOut button
                platformInverted: window.platformInverted
                width: window.inPortrait ? parent.width/2 : parent.height/2
                text: qsTr("Sign Out")
                anchors {horizontalCenter: parent.horizontalCenter}
                onClicked: db.restoreAuth()
            }



            Separator{}


            Label{
                platformInverted: window.platformInverted
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: platformStyle.fontSizeLarge*1.2
                text: qsTr("Sync settings");
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter;

                Label{
                    platformInverted: window.platformInverted
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Auto sync <font size=\"2\">(startup, refresh cache)</font>");
                    font.pixelSize: platformStyle.fontSizeLarge
                    width: parent.width - syncSwitch.width-5
                }

                Switch {
                    id: syncSwitch
                    platformInverted: window.platformInverted
                    checked: db.sync
                    onCheckedChanged: {
                        db.sync = checked
                    }
                }
            }



            Separator{}


            Label{
                platformInverted: window.platformInverted
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: platformStyle.fontSizeLarge*1.2
                text: qsTr("Theme settings");
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter;

                Label{
                    platformInverted: window.platformInverted
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Night mode");
                    font.pixelSize: platformStyle.fontSizeLarge
                    width: parent.width - themeSwitch.width-5
                }

                Switch {
                    id: themeSwitch
                    platformInverted: window.platformInverted
                    checked: !db.theme
                    onCheckedChanged: {
                        db.theme = !checked
                    }
                }
            }

            Separator{}


            Label{
                platformInverted: window.platformInverted
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: platformStyle.fontSizeLarge*1.2
                text: qsTr("Article settings");
            }


            SelectionListItem {
                id: fontSize
                platformInverted: window.platformInverted
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: -8
                anchors.rightMargin: -8
                title: qsTr("Font Size");
                onClicked: fontSizeDialog.open()

                SelectionDialog {
                    id: fontSizeDialog
                    platformInverted: window.platformInverted
                    titleText: qsTr("Font Size");
                    model: ListModel {
                        ListElement { name: "Small"}
                        ListElement { name: "Medium"}
                        ListElement { name: "Large"}
                    }
                    onSelectedIndexChanged: {
                        fontSize.subTitle = fontSizeDialog.model.get(selectedIndex).name
                        switch (selectedIndex){
                        case 0:
                            db.font = platformStyle.fontSizeSmall;
                            break;
                        case 1:
                            db.font = platformStyle.fontSizeMedium+2;
                            break;
                        case 2:
                            db.font = platformStyle.fontSizeLarge+4;
                            break;
                        }
                    }
                }
            }


            Separator{}


            Label{
                platformInverted: window.platformInverted
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: platformStyle.fontSizeLarge*1.2
                text: qsTr("Sorting settings");
            }


            SelectionListItem {
                id: orderSettings
                platformInverted: window.platformInverted
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: -8
                anchors.rightMargin: -8
                title: qsTr("Order by");
                onClicked: orderDialog.open()

                SelectionDialog {
                    id: orderDialog
                    platformInverted: window.platformInverted
                    titleText: qsTr("Order by");
                    model: ListModel {
                        ListElement { name: "Title"}
                        ListElement { name: "Favorite"}
                        ListElement { name: "Readed"}
                    }
                    onSelectedIndexChanged: {
                        orderSettings.subTitle = orderDialog.model.get(selectedIndex).name
                        switch (selectedIndex){
                        case 0:
                            db.sortBy = "title"
                            bModel.sortingBy = db.sortBy
                            break;
                        case 1:
                            db.sortBy = "favorite"
                            bModel.sortingBy = db.sortBy
                            break;
                        case 2:
                            db.sortBy = "archive"
                            bModel.sortingBy = db.sortBy
                            break;
                        }
                    }
                }
            }


            SelectionListItem {
                id: sortOrderSettings
                platformInverted: window.platformInverted
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: -8
                anchors.rightMargin: -8
                title: qsTr("Sort order");
                onClicked: sortOrderDialog.open()

                SelectionDialog {
                    id: sortOrderDialog
                    platformInverted: window.platformInverted
                    titleText: qsTr("Sort order");
                    model: ListModel {
                        ListElement { name: "Ascending"}
                        ListElement { name: "Descending"}
                    }
                    onSelectedIndexChanged: {
                        sortOrderSettings.subTitle = sortOrderDialog.model.get(selectedIndex).name
                        switch (selectedIndex){
                        case 0:
                            db.sortOrder = "ascending"
                            bModel.sortingOrder = db.sortOrder
                            break;
                        case 1:
                            db.sortOrder = "descending"
                            bModel.sortingOrder = db.sortOrder
                            break;
                        }
                    }
                }
            }

            Separator{}


            Label{
                platformInverted: window.platformInverted
                anchors {horizontalCenter: parent.horizontalCenter}
                font.pixelSize: platformStyle.fontSizeLarge*1.2
                text: qsTr("Debugging");
            }

            Row{
                spacing: 5
                width: parent.width
                anchors.horizontalCenter: parent.horizontalCenter;

                Label{
                    platformInverted: window.platformInverted
                    anchors.verticalCenter: parent.verticalCenter
                    text: qsTr("Show server side errors");
                    font.pixelSize: platformStyle.fontSizeLarge
                    width: parent.width - debugSwitch.width-5
                }

                Switch {
                    id: debugSwitch
                    platformInverted: window.platformInverted
                    checked: db.debug
                    onCheckedChanged: {
                        db.debug = checked
                    }
                }
            }

        }

    }
}
