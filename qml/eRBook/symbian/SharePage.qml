// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import com.nokia.symbian 1.1


import "Component"
import "Delegate"
import "../common"
import "../scripts/createobject.js" as Dynamic
import "../scripts/readability.js" as Readability

MyPage{
    id: root
    tools: ToolBarLayout{
        ToolButton{
            flat: true
            platformInverted: window.platformInverted
            iconSource: "toolbar-back"
            onClicked: root.pageStack.pop()
        }
    }


    headerText: qsTr("Share")
    onStatusChanged: {
        if (status == PageStatus.Active){
            if (!db.shortUrlExists(bModel.cArticleId)){
                api.shortUrl(bModel.cUrl)
            }
        }
    }


    Label{
        id: articleTitle
        z: 1000
        platformInverted: window.platformInverted
        anchors {left: parent.left; right: parent.right; top: parent.top; topMargin: headerHeight; margins: platformStyle.paddingMedium}
        maximumLineCount: 3
        wrapMode: Text.WordWrap
        elide: Text.ElideRight
        font.pixelSize: platformStyle.fontSizeLarge
        text: bModel.cTitle
    }

    Separator{
        z: 1000
        id: separe
        anchors {
            top: articleTitle.bottom
        }
    }


    Flickable{
        id: flicker
        anchors {
            left: parent.left
            right: parent.right
            top: separe.bottom
            bottom: parent.bottom
            margins: platformStyle.paddingMedium}
        //flickableDirection: Flickable.VerticalFlick
        clip: true
        contentHeight: content.height


        Column{
            id: content
            width: parent.width
            spacing: 5

            Button{
                platformInverted: window.platformInverted
                anchors {horizontalCenter: parent.horizontalCenter}
                width: parent.width * 0.8
                text: qsTr("SMS")
                onClicked: Qt.openUrlExternally("sms:?body="+bModel.cTitle+"\nshared with #eRBook\n"+db.shortUrl(bModel.cArticleId))
            }
            Button{
                platformInverted: window.platformInverted
                anchors {horizontalCenter: parent.horizontalCenter}
                width: parent.width * 0.8
                text: qsTr("Email")
                onClicked: Qt.openUrlExternally("mailto:?subject=&body="+bModel.cTitle+"\nshared with #eRBook\n"+db.shortUrl(bModel.cArticleId))
            }
            Button{
                platformInverted: window.platformInverted
                anchors {horizontalCenter: parent.horizontalCenter}
                width: parent.width * 0.8
                text: qsTr("Twitter")
                onClicked: {
                    var url = "https://twitter.com/intent/tweet?url="+encodeURIComponent(db.shortUrl(bModel.cArticleId))+
                            "&hashtags="+encodeURIComponent("eRBook, symbian")
                    console.debug(url)
                    Qt.openUrlExternally(url)
                }
            }
        }
    }


}
