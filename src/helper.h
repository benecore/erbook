#ifndef HELPER_H
#define HELPER_H

#include <QObject>
#include <QClipboard>
#include <QApplication>
#include <QDesktopServices>
#include <QUrl>

class Helper : public QObject
{
    Q_OBJECT
public:
    explicit Helper(QObject *parent = 0);

signals:
    void copied();


public slots:
    QString getText();
    void setText(const QString &text);
    void sendEmail(const QString &body);
    void sendSms(const QString &body);
    void openUrl(const QString &url);


private:
    QClipboard *clipboard;
};

#endif // HELPER_H
