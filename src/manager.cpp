#include "manager.h"

Manager::Manager(QObject *parent) :
    QObject(parent), _syncing(false)
{
    manager = new QNetworkAccessManager(this);
    set = new Settings(this);
    auth = new OAuth("Benecore2", "gXahkxWqxmCQKbjvZkxDztBZkrNYMfaf", this);
    reader = new JsonReader;
    model = new Model;
    sortModel = new SortModel(this);
    sortModel->setSourceModel(model);
}



void Manager::handleErrors(const int &errorCode)
{
    switch (errorCode){
    case 400:
        emit error("[400] Bad Request",
                   "The server could not understand your request. Verify that request parameters (and content, if any) are valid.");
        break;
    case 401:
        emit error("[401] Authorization Required",
                   "Authentication failed or was not provided. Verify that you have sent valid credentials.");
        break;
    case 403:
        emit error("[403] Forbidden",
                   "The server understood your request and verified your credentials, but you are not allowed to perform the requested action.");
        break;
    case 404:
        emit error("[404] Not Found",
                   "The resource that you requested does not exist.");
        break;
    case 409:
        emit error("[409] Conflict",
                   "The resource that you are trying to create already exists. This should also provide a Location header to the resource in question.");
        break;
    case 500:
        emit error("[500] Internal Server Error",
                   "An unknown error has occurred.");
        break;
    }
}



void Manager::requestToken()
{
    QUrl url("https://www.readability.com/api/rest/v1/oauth/request_token/");

    auth->clearTokens();
    QByteArray authHeader = auth->generateAuthorizationHeader(url, OAuth::POST);

    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    req.setRawHeader("Authorization ", authHeader);


    QNetworkReply *reply = manager->post(req, QByteArray());
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(requestTokenFinished()));
}




void Manager::requestTokenFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    if (reply){
        if (reply->error() == QNetworkReply::NoError){
            QByteArray parseQuery("http://parse.com?");
            QUrl parseUrl = QUrl::fromEncoded(parseQuery + reply->readAll());
            auth->setOAuthToken(parseUrl.encodedQueryItemValue("oauth_token"));
            auth->setOAuthTokenSecret(parseUrl.encodedQueryItemValue("oauth_token_secret"));
            QUrl response(QString("https://www.readability.com/api/rest/v1/oauth/authorize"));
            response.addQueryItem("oauth_token", auth->oauthToken());
            response.addQueryItem("oauth_callback", "localhost");
            emit requestTokenDone(response);
        }else{
            handleErrors(errorCode);
            qDebug() << "ERROR CODE:" << errorCode;
            qDebug() << "ERROR RESPONSE:" << reply->errorString();
            qDebug() << "ERROR RESPONSE:" << reply->readAll();
        }
    }
    reply->deleteLater();
}



void Manager::accessToken(const QUrl &webUrl)
{
    QString verifier = webUrl.encodedQueryItemValue("oauth_verifier");

    QUrl url("https://www.readability.com/api/rest/v1/oauth/access_token");
    url.addQueryItem("oauth_verifier", verifier.toUtf8());

    //qDebug() << "TOKEN" << auth->oauthToken() << "SECRET:" << auth->oauthTokenSecret();
    auth->setOAuthToken(auth->oauthToken());
    auth->setOAuthTokenSecret(auth->oauthTokenSecret());
    QByteArray authHeader = auth->generateAuthorizationHeader(url, OAuth::GET);
    //qDebug() << authHeader;

    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    req.setRawHeader("Authorization ", authHeader);


    QNetworkReply *reply = manager->get(req);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(accessTokenFinished()));
}




void Manager::accessTokenFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    if (reply){
        if (reply->error() == QNetworkReply::NoError){
            //qDebug() << reply->readAll();
            QByteArray parseQuery("http://parse.com?");
            QUrl parseUrl = QUrl::fromEncoded(parseQuery + reply->readAll());
            set->setAuth(parseUrl.encodedQueryItemValue("oauth_token"),
                         parseUrl.encodedQueryItemValue("oauth_token_secret"));
            emit accessTokenDone();
        }else{
            handleErrors(errorCode);
            qDebug() << "ERROR CODE:" << errorCode;
            qDebug() << "ERROR RESPONSE:" << reply->errorString();
            qDebug() << "ERROR RESPONSE:" << reply->readAll();
        }
    }
    reply->deleteLater();
}



/* All request
  *
  *
  *
  *
  *
  *
  *
*/


void Manager::offlineBookmarks()
{
    QList<QVariantMap> temp = set->getBookmarks();
    for (int i = 0; i < temp.count(); i++){
        sortModel->appendOffline(temp[i]);
    }
    emit bookmarksDone();
}



void Manager::bookmarks(const QVariant &page, const QVariant &per_page)
{
    QUrl url("https://www.readability.com/api/rest/v1/bookmarks");
    url.addQueryItem("page", page.toString());
    url.addQueryItem("per_page", per_page.toString());

    auth->setOAuthToken(set->getToken().toString().toUtf8());
    auth->setOAuthTokenSecret(set->getSecret().toString().toUtf8());
    QByteArray authHeader = auth->generateAuthorizationHeader(url, OAuth::GET);


    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    req.setRawHeader("Authorization ", authHeader);

    QNetworkReply *reply = manager->get(req);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(bookmarksFinished()));
}




void Manager::bookmarksFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    if (reply){
        if (reply->error() == QNetworkReply::NoError){
            reader->parse(reply->readAll());
            foreach (QVariant temp, reader->result().toMap()["bookmarks"].toList()){
                if (!set->bookmarkExists(temp.toMap()["article"].toMap()["id"].toString())){
                    set->setBookmarkCache(temp.toMap());
                }
            }
            _totalArticles = reader->result().toMap()["meta"].toMap()["item_count_total"].toInt();
            _totalPage = reader->result().toMap()["meta"].toMap()["num_pages"].toInt();
            _actualPage = reader->result().toMap()["meta"].toMap()["page"].toInt();
            emit metaDataChanged();
            //offlineBookmarks();
            qDebug() << "ACTUAL PAGE: " << _actualPage << "\nTOTAL PAGE: " << _totalPage;
            if (_actualPage == _totalPage){
                syncingStart();
            }
            else{
                _actualPage++;
                bookmarks(QVariant(_actualPage));
            }
        }else{
            handleErrors(errorCode);
            qDebug() << "ERROR CODE:" << errorCode;
            qDebug() << "ERROR STRING:" << reply->errorString();
            qDebug() << "ERROR RESPONSE:" << reply->readAll();
        }
    }
    reply->deleteLater();
}



void Manager::readBookmark(const QVariant &id)
{
    if (id.toString().isEmpty())
        QUrl url(QString("https://www.readability.com/api/rest/v1/articles/%1").arg(sortModel->cArticleId()));
    QUrl url(QString("https://www.readability.com/api/rest/v1/articles/%1").arg(id.toString()));

    auth->setOAuthToken(set->getToken().toString().toUtf8());
    auth->setOAuthTokenSecret(set->getSecret().toString().toUtf8());
    QByteArray authHeader = auth->generateAuthorizationHeader(url, OAuth::GET);

    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    req.setRawHeader("Authorization ", authHeader);
    qDebug() << authHeader;

    QNetworkReply *reply = manager->get(req);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(readBookmarkFinished()));
}




void Manager::readBookmarkFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    if (reply){
        if (reply->error() == QNetworkReply::NoError){
            emit contentDone();
            bookmarkContentDone(reply->readAll());
        }
        else{
            handleErrors(errorCode);
            qDebug() << "ERROR CODE:" << errorCode;
            qDebug() << "ERROR STRING:" << reply->errorString();
            qDebug() << "ERROR RESPONSE:" << reply->readAll();
        }
    }
    reply->deleteLater();
}




void Manager::addBookmark(const QString &urlString, const int &favorite, const int &archive)
{
    QUrl url("https://www.readability.com/api/rest/v1/bookmarks/");
    url.addEncodedQueryItem("url", QUrl::toPercentEncoding(urlString.toUtf8()));
    url.addQueryItem("favorite", QString::number(favorite).toUtf8());
    url.addQueryItem("archive", QString::number(archive).toUtf8());

    auth->setOAuthToken(set->getToken().toString().toUtf8());
    auth->setOAuthTokenSecret(set->getSecret().toString().toUtf8());
    QByteArray authHeader = auth->generateAuthorizationHeader(url, OAuth::POST);

    QByteArray postData;
    postData += "url="+QUrl::toPercentEncoding(urlString.toUtf8());
    postData += "&favorite="+QString::number(favorite).toUtf8();
    postData += "&archive="+QString::number(archive).toUtf8();


    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    req.setRawHeader("Authorization", authHeader);

    QNetworkReply *reply = manager->post(req, postData);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(addBookmarkFinished()));
}



void Manager::addBookmarkFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    if (reply){
        if (reply->error() == QNetworkReply::NoError){
            emit addBookmarkDone(reply->readAll());
        }
        else{
            QString temp = reply->readAll();
            if (temp.contains("Bookmark already exists")){
                emit banner(tr("Article already exists"));
            }
            emit errorAddBookmark();
            //handleErrors(errorCode);
            qDebug() << "ERROR CODE:" << errorCode;
            qDebug() << "ERROR STRING:" << reply->errorString();
            qDebug() << "ERROR RESPONSE:" << reply->readAll();
        }
    }
    reply->deleteLater();
}



void Manager::deleteBookmark()
{
    QUrl url(QString("https://www.readability.com/api/rest/v1/bookmarks/%1").arg(sortModel->cId()));

    auth->setOAuthToken(set->getToken().toString().toUtf8());
    auth->setOAuthTokenSecret(set->getSecret().toString().toUtf8());
    QByteArray authHeader = auth->generateAuthorizationHeader(url, OAuth::DELETE);


    QNetworkRequest req(url);
    req.setRawHeader("Authorization", authHeader);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");


    QNetworkReply *reply = manager->deleteResource(req);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(deleteBookmarkFinished()));
}



void Manager::deleteBookmarkFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    if (reply){
        if (reply->error() == QNetworkReply::NoError){
            emit deleteBookmarkDone(reply->readAll());
        }else{
            handleErrors(errorCode);
            qDebug() << "\nERROR CODE:" << errorCode;
            qDebug() << "ERROR STRING:" << reply->errorString();
            qDebug() << "ERROR RESPONSE:" << reply->readAll();
        }
    }
    reply->deleteLater();
}



void Manager::updateBookmark(const int &favorite, const int &archive)
{
    QUrl url(QString("https://www.readability.com/api/rest/v1/bookmarks/%1").arg(sortModel->cId()));
    url.addQueryItem("favorite", QString::number(favorite).toUtf8());
    url.addQueryItem("archive", QString::number(archive).toUtf8());

    auth->setOAuthToken(set->getToken().toString().toUtf8());
    auth->setOAuthTokenSecret(set->getSecret().toString().toUtf8());
    QByteArray authHeader = auth->generateAuthorizationHeader(url, OAuth::POST);

    QByteArray postData;
    postData += "favorite="+QString::number(favorite).toUtf8();
    postData += "&archive="+QString::number(archive).toUtf8();

    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");
    req.setRawHeader("Authorization", authHeader);

    QNetworkReply *reply = manager->post(req, postData);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(updateBookmarkFinished()));
}



void Manager::updateBookmarkFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    if (reply){
        if (reply->error() == QNetworkReply::NoError){
            emit updateBookmarkDone(reply->readAll());
        }else{
            handleErrors(errorCode);
            qDebug() << "\nERROR CODE:" << errorCode;
            qDebug() << "ERROR STRING:" << reply->errorString();
            qDebug() << "ERROR RESPONSE:" << reply->readAll();
        }
    }
    reply->deleteLater();
}



void Manager::userInfo()
{
    QUrl url("https://www.readability.com/api/rest/v1/users/_current");

    auth->setOAuthToken(set->getToken().toString().toUtf8());
    auth->setOAuthTokenSecret(set->getSecret().toString().toUtf8());
    QByteArray authHeader = auth->generateAuthorizationHeader(url, OAuth::GET);


    QNetworkRequest req(url);
    req.setRawHeader("Authorization", authHeader);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");


    QNetworkReply *reply = manager->get(req);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(userInfoFinished()));
}



void Manager::userInfoFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    if (reply){
        if (reply->error() == QNetworkReply::NoError){
            emit userInfoDone(reply->readAll());
        }else{
            handleErrors(errorCode);
            qDebug() << "\nERROR CODE:" << errorCode;
            qDebug() << "ERROR STRING:" << reply->errorString();
            qDebug() << "ERROR RESPONSE:" << reply->readAll();
        }
    }
    reply->deleteLater();
}



void Manager::shortUrl(const QString &sourceUrl)
{
    QUrl url("http://www.readability.com/api/shortener/v1/urls");

    QByteArray postData;
    postData += "url="+QUrl::toPercentEncoding(sourceUrl);


    QNetworkRequest req(url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");


    QNetworkReply *reply = manager->post(req, postData);
    reply->ignoreSslErrors();
    connect(reply, SIGNAL(finished()), this, SLOT(shortUrlFinished()));
}



void Manager::syncingStart()
{
    setSyncing(true);

    sortModel->clear();
    QList<QVariantMap> list = set->getBookmarks();
    for (int i = 0; i < list.count(); ++i){
        if (!set->contentExists(list.at(i).value("id"))){
            qDebug() << "SETTING CONTENT";
            QEventLoop loop;
            loop.connect(this, SIGNAL(contentDone()), SLOT(quit()));
            readBookmark(list.at(i).value("id"));
            loop.exec();
        }
        emit currentSyncing(i, list.count());
    }/*
    foreach(QVariantMap article, list){
        if (!set->contentExists(article["id"])){
            qDebug() << "SETTING CONTENT";
            QEventLoop loop;
            loop.connect(this, SIGNAL(contentDone()), SLOT(quit()));
            readBookmark(article["id"]);
            loop.exec();
        }
    }*/
    qDebug() << "KONIEC SYNCHRONIZACIE";
    setSyncing(false);
    offlineBookmarks();
}



void Manager::bookmarkContentDone(const QByteArray result)
{
    if (!_syncing)
        emit readBookmarkDone(result);
    reader->parse(result);
    set->setBookmark(reader->result().toMap()["id"], reader->result().toMap()["content"]);

}



void Manager::shortUrlFinished()
{
    QNetworkReply *reply = qobject_cast<QNetworkReply*>(sender());
    int errorCode = reply->attribute( QNetworkRequest::HttpStatusCodeAttribute ).toInt();
    if (reply){
        if (reply->error() == QNetworkReply::NoError){
            reader->parse(reply->readAll());
            //qDebug() << reader->result().toMap()["meta"].toMap()["rdd_url"].toString();
            emit shortUrlDone(reader->result().toMap()["meta"].toMap()["rdd_url"].toString());
        }else{
            //handleErrors(errorCode);
            emit shortUrlError();
            qDebug() << "\nERROR CODE:" << errorCode;
            qDebug() << "ERROR STRING:" << reply->errorString();
            qDebug() << "ERROR RESPONSE:" << reply->readAll();
        }
    }
    reply->deleteLater();
}
