#ifndef MODELITEM_H
#define MODELITEM_H

#include <QObject>
#include <QVariantMap>
#include <QDebug>

class ModelItem : public QObject
{
    Q_OBJECT
    Q_PROPERTY(bool favorite READ favorite NOTIFY dataChanged)
    Q_PROPERTY(bool archive READ archive NOTIFY dataChanged)
    Q_PROPERTY(bool processed READ processed NOTIFY dataChanged)
    Q_PROPERTY(QString domain READ domain NOTIFY dataChanged)
    Q_PROPERTY(QString title READ title NOTIFY dataChanged)
    Q_PROPERTY(QString url READ url NOTIFY dataChanged)
    Q_PROPERTY(QString lead_image READ lead_image NOTIFY dataChanged)
    Q_PROPERTY(QString author READ author NOTIFY dataChanged)
    Q_PROPERTY(QString excerpt READ excerpt NOTIFY dataChanged)
    Q_PROPERTY(QString word_count READ word_count NOTIFY dataChanged)
    Q_PROPERTY(QString article_id READ article_id NOTIFY dataChanged)
    Q_PROPERTY(QString id READ id NOTIFY dataChanged)
    Q_PROPERTY(QString date_added READ date_added NOTIFY dataChanged)
public:
    explicit ModelItem(const QVariantMap &map);
    explicit ModelItem(const QVariantMap &map, const int offline);

signals:
    void dataChanged();

public slots:
    inline bool favorite() const { return m_favorite; }
    inline bool archive() const { return m_archive; }
    inline bool processed() const { return m_processed; }
    inline QString domain() const { return m_domain; }
    inline QString title() const { return m_title; }
    inline QString url() const { return m_url; }
    inline QString lead_image() const { return m_lead_image_url; }
    inline QString author() const { return m_author; }
    inline QString excerpt() const { return m_excerpt; }
    inline QString word_count() const { return m_word_count; }
    inline QString article_id() const { return m_article_id; }
    inline QString id() const { return m_id; }
    inline QString date_added() const { return m_date_added; }

    //: SET
    inline void setFavorite(const bool &value) { m_favorite = value; emit dataChanged(); }
    inline void setArchive(const bool &value) { m_archive = value; emit dataChanged(); }
    /*
    inline void setProcessed(const bool &value) { return m_processed; }
    inline void setDomain(const QString &value) { return m_domain; }
    inline void setTitle(const QString &value) { return m_title; }
    inline void setUrl(const QString &value) { return m_url; }
    inline void setLead_image(const QString &value) { return m_lead_image_url; }
    inline void setAuthor(const QString &value) { return m_author; }
    inline void setExcerpt(const QString &value) { return m_excerpt; }
    inline void setWord_count(const QString &value) { return m_word_count; }
    inline void setArticle_id(const QString &value) { return m_article_id; }
    inline void setId(const QString &value) { return m_id; }
    inline void setDate_added(const QString &value) { return m_date_added; }
    */


private:
    bool m_favorite,
    m_archive,
    m_processed;
    QString m_domain,
    m_title,
    m_url,
    m_lead_image_url,
    m_author,
    m_excerpt,
    m_word_count,
    m_article_id,
    m_id,
    m_date_added;

};

#endif // MODELITEM_H
