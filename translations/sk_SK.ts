<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="sk_SK">
<context>
    <name>AboutPage</name>
    <message>
        <location filename="../qml/eRBook/harmattan/AboutPage.qml" line="24"/>
        <location filename="../qml/eRBook/symbian/AboutPage.qml" line="24"/>
        <source>About</source>
        <translation>O aplikácii</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AboutPage.qml" line="53"/>
        <location filename="../qml/eRBook/symbian/AboutPage.qml" line="54"/>
        <source>eRBook</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AboutPage.qml" line="58"/>
        <location filename="../qml/eRBook/symbian/AboutPage.qml" line="60"/>
        <source>version %1</source>
        <translation>verzia %1</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AboutPage.qml" line="63"/>
        <location filename="../qml/eRBook/symbian/AboutPage.qml" line="67"/>
        <source>Copyright (c) 2010-2013 DevPDA&lt;br/&gt;&lt;a  href=&apos;http://devpda.net&apos;&gt;www.devpda.net&lt;/a&gt;</source>
        <translation>Autorské práva (c) 2010-2013 DevPDA&lt;br/&gt;&lt;a  href=&apos;http://devpda.net&apos;&gt;www.devpda.net&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AboutPage.qml" line="72"/>
        <location filename="../qml/eRBook/symbian/AboutPage.qml" line="77"/>
        <source>A fully featured Readability client for Symbian and MeeGo Smartphones that allows you to take your reading list.</source>
        <translation>eRBook je mobilná aplikácia využivajúca API rozhranie Readability.com. S eRBook máte svoje články uložené pre neskoršie čítanie vždy po ruke.</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AboutPage.qml" line="80"/>
        <location filename="../qml/eRBook/symbian/AboutPage.qml" line="87"/>
        <source>Privacy policy</source>
        <translation>Ochrana osobných údajov</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AboutPage.qml" line="86"/>
        <location filename="../qml/eRBook/symbian/AboutPage.qml" line="94"/>
        <source>eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook.</source>
        <translation>eRBook ukladá len prístupový token. Prístupový token je uložený keď udelíte prístup k svojmu Readability účtu. Prístupový token je uložený lokálne vo vašom telefóne a je odstránený pri odinštalovaní aplikácie.</translation>
    </message>
</context>
<context>
    <name>AddBookmark</name>
    <message>
        <location filename="../qml/eRBook/harmattan/AddBookmark.qml" line="28"/>
        <location filename="../qml/eRBook/symbian/AddBookmark.qml" line="29"/>
        <source>Add article</source>
        <translation>Pridať článok</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AddBookmark.qml" line="77"/>
        <location filename="../qml/eRBook/symbian/AddBookmark.qml" line="80"/>
        <source>Article Url</source>
        <translation>URL článku</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AddBookmark.qml" line="83"/>
        <location filename="../qml/eRBook/symbian/AddBookmark.qml" line="87"/>
        <source>required</source>
        <translation>povinné</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AddBookmark.qml" line="93"/>
        <location filename="../qml/eRBook/symbian/AddBookmark.qml" line="98"/>
        <source>Paste Url</source>
        <translation>Prilepiť url</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AddBookmark.qml" line="95"/>
        <location filename="../qml/eRBook/symbian/AddBookmark.qml" line="100"/>
        <source>Clear</source>
        <translation>Vymazať</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AddBookmark.qml" line="117"/>
        <location filename="../qml/eRBook/symbian/AddBookmark.qml" line="123"/>
        <source>Favorite</source>
        <translation>Obľúbený</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AddBookmark.qml" line="147"/>
        <location filename="../qml/eRBook/symbian/AddBookmark.qml" line="155"/>
        <source>Archive</source>
        <translation>Prečítaný</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AddBookmark.qml" line="166"/>
        <location filename="../qml/eRBook/symbian/AddBookmark.qml" line="176"/>
        <source>Add Article</source>
        <translation>Pridať článok</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/AddBookmark.qml" line="170"/>
        <location filename="../qml/eRBook/symbian/AddBookmark.qml" line="180"/>
        <source>Adding article</source>
        <translation>Odosielam článok</translation>
    </message>
</context>
<context>
    <name>AuthPage</name>
    <message>
        <location filename="../qml/eRBook/harmattan/AuthPage.qml" line="26"/>
        <location filename="../qml/eRBook/symbian/AuthPage.qml" line="45"/>
        <source>Sign In</source>
        <translation>Prihlásenie</translation>
    </message>
</context>
<context>
    <name>Context</name>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/Context.qml" line="24"/>
        <location filename="../qml/eRBook/symbian/Component/Context.qml" line="25"/>
        <source>Remove from favorite</source>
        <translation>Odobrať z obľúbených</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/Context.qml" line="24"/>
        <location filename="../qml/eRBook/symbian/Component/Context.qml" line="25"/>
        <source>Add to favorite</source>
        <translation>Pridať k obľúbeným</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/Context.qml" line="28"/>
        <location filename="../qml/eRBook/symbian/Component/Context.qml" line="30"/>
        <source>Set as unread</source>
        <translation>Nastaviť ako neprečítané</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/Context.qml" line="28"/>
        <location filename="../qml/eRBook/symbian/Component/Context.qml" line="30"/>
        <source>Set as read</source>
        <translation>Nastaviť ako prečítané</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/Context.qml" line="32"/>
        <location filename="../qml/eRBook/symbian/Component/Context.qml" line="35"/>
        <source>Remove</source>
        <translation>Odstrániť</translation>
    </message>
</context>
<context>
    <name>DeleteDialog</name>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/DeleteDialog.qml" line="9"/>
        <location filename="../qml/eRBook/symbian/Component/DeleteDialog.qml" line="11"/>
        <source>Delete</source>
        <translation>Odstrániť</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/DeleteDialog.qml" line="10"/>
        <location filename="../qml/eRBook/symbian/Component/DeleteDialog.qml" line="12"/>
        <source>Cancel</source>
        <translation>Zrušiť</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/DeleteDialog.qml" line="14"/>
        <location filename="../qml/eRBook/symbian/Component/DeleteDialog.qml" line="16"/>
        <source>Are you sure you want to delete this article</source>
        <translation>Naozaj chceš odstrániť článok</translation>
    </message>
</context>
<context>
    <name>FilteringDialog</name>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/FilteringDialog.qml" line="8"/>
        <location filename="../qml/eRBook/symbian/Component/FilteringDialog.qml" line="8"/>
        <source>View mode</source>
        <translation>Zobraziť</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/FilteringDialog.qml" line="31"/>
        <location filename="../qml/eRBook/symbian/Component/FilteringDialog.qml" line="32"/>
        <source>All Items</source>
        <translation>Všetky</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/FilteringDialog.qml" line="32"/>
        <location filename="../qml/eRBook/symbian/Component/FilteringDialog.qml" line="33"/>
        <source>Read items</source>
        <translation>Prečítané</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/Component/FilteringDialog.qml" line="33"/>
        <location filename="../qml/eRBook/symbian/Component/FilteringDialog.qml" line="34"/>
        <source>Unread items</source>
        <translation>Neprečítané</translation>
    </message>
</context>
<context>
    <name>ListDelegate</name>
    <message>
        <location filename="../qml/eRBook/harmattan/Delegate/ListDelegate.qml" line="95"/>
        <location filename="../qml/eRBook/symbian/Delegate/ListDelegate.qml" line="85"/>
        <source>readed</source>
        <translation>prečítaný</translation>
    </message>
</context>
<context>
    <name>ListPage</name>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="44"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="57"/>
        <source>Articles</source>
        <translation>Články</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="60"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="75"/>
        <source>Sync articles</source>
        <translation>Synchronizovať</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="62"/>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="152"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="77"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="168"/>
        <source>Syncing</source>
        <translation>Synchronizácia</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="68"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="84"/>
        <source>About</source>
        <translation>O aplikácii</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="90"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="106"/>
        <source>Removing article</source>
        <translation>Odstraňujem článok</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="100"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="116"/>
        <source>Set as unread</source>
        <translation>Nastavujem ako neprečítaný</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="100"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="116"/>
        <source>Set as read</source>
        <translation>Nastavujem ako prečítaný</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="108"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="124"/>
        <source>Remove from favorite</source>
        <translation>Odstraňujem z obľúbených</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="108"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="124"/>
        <source>Add to favorite</source>
        <translation>Pridať k obľúbeným</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="124"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="140"/>
        <source>No articles</source>
        <translation>Žiadne články</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="127"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="143"/>
        <source>No read articles</source>
        <translation>Źiadne prečítané</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="130"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="146"/>
        <source>No unread articles</source>
        <translation>Žiadne neprečítané</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListPage.qml" line="146"/>
        <location filename="../qml/eRBook/symbian/ListPage.qml" line="162"/>
        <source>Loading content</source>
        <translation>Načítanie obsahu</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="14"/>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="156"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="14"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="186"/>
        <source>Sign In</source>
        <translation>Prihlásenie</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="26"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="34"/>
        <source>Syncing</source>
        <translation>Synchronizácia</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="30"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="38"/>
        <source>Invalid credentials</source>
        <translation>Neplatné prihlasovacie údaje</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="52"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="61"/>
        <source>eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook.</source>
        <translation>eRBook ukladá len prístupový token. Prístupový token je uložený keď udelíte prístup k svojmu Readability účtu. Prístupový token je uložený lokálne vo vašom telefóne a je odstránený pri odinštalovaní aplikácie.</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="53"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="62"/>
        <source>Privacy policy</source>
        <translation>Ochrana osobných údajov</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="105"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="127"/>
        <source>eRBook %1</source>
        <translation>eRBook %1</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="113"/>
        <source>need access to you Readability account. Fill the fields and click on Sign In button.</source>
        <translation>potrebuje prístup k tvôjmu Readability.com účtu. Vyplň povinné polia a klikni na Prihlásiť.</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="118"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="144"/>
        <source>Username</source>
        <translation>Uživateľské meno</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="125"/>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="142"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="152"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="171"/>
        <source>required</source>
        <translation>povinné</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="135"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="163"/>
        <source>Password</source>
        <translation>Heslo</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="160"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="189"/>
        <source>Please wait</source>
        <translation>Čakaj prosím</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="170"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="199"/>
        <source>Sign Up</source>
        <translation>Registrovať</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPage.qml" line="180"/>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="226"/>
        <source>&lt;a style=&apos;color:red&apos; href=&apos;http://www.slavnecitaty.eu&apos;&gt;Privacy policy&lt;/a&gt;</source>
        <translation>&lt;a style=&apos;color:red&apos; href=&apos;http://www.slavnecitaty.eu&apos;&gt;Ochrana osobných údajov&lt;/a&gt;</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="136"/>
        <source>need access to you Readability account.</source>
        <translation>potrebuje prístup k tvôjmu Readability účtu.</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="137"/>
        <source>If you haven&apos;t account you can create one. Just click on the Sign Up button.</source>
        <translation>Ak nemáš účet, môžeš si ho jednoducho vytvoriť kliknutím na Registrovať.</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/symbian/LoginPage.qml" line="138"/>
        <source>Then fill the required fields and click on the Sign In button.</source>
        <translation>Potom vyplň povinné polia a klikni na Prihlásiť.</translation>
    </message>
</context>
<context>
    <name>LoginPageTEMP</name>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPageTEMP.qml" line="13"/>
        <location filename="../qml/eRBook/harmattan/LoginPageTEMP.qml" line="82"/>
        <source>Sign In</source>
        <translation>Prihlásiť</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPageTEMP.qml" line="23"/>
        <source>Please wait</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPageTEMP.qml" line="64"/>
        <source>eRBook %1</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPageTEMP.qml" line="72"/>
        <source>need access to you Readability account. Just click on Sign In button.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPageTEMP.qml" line="93"/>
        <source>Sign Up</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPageTEMP.qml" line="103"/>
        <source>Privacy policy</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/LoginPageTEMP.qml" line="114"/>
        <source>eRBook store only access token. Access token are stored when you grant access to you Readability account. Access token is stored locally on your phone and is deleted when you uninstall eRBook.</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Manager</name>
    <message>
        <location filename="../src/manager.cpp" line="295"/>
        <source>Article already exists</source>
        <translation>Článok už existuje</translation>
    </message>
</context>
<context>
    <name>PullDownRefreshHeader</name>
    <message>
        <location filename="../qml/eRBook/harmattan/ListComp/PullDownRefreshHeader.qml" line="43"/>
        <location filename="../qml/eRBook/symbian/ListComp/PullDownRefreshHeader.qml" line="41"/>
        <source>Release to sync</source>
        <translation>Pusť pre synchronizáciu</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ListComp/PullDownRefreshHeader.qml" line="43"/>
        <location filename="../qml/eRBook/symbian/ListComp/PullDownRefreshHeader.qml" line="41"/>
        <source>Pull down to sync</source>
        <translation>Potiahni nadol pre synchronizáciu</translation>
    </message>
</context>
<context>
    <name>ReadPage</name>
    <message>
        <location filename="../qml/eRBook/harmattan/ReadPage.qml" line="25"/>
        <source>Share</source>
        <translation>Zdieľať</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ReadPage.qml" line="35"/>
        <location filename="../qml/eRBook/symbian/ReadPage.qml" line="37"/>
        <source>Remove from favorite</source>
        <translation>Odstrániť článok</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ReadPage.qml" line="37"/>
        <location filename="../qml/eRBook/symbian/ReadPage.qml" line="39"/>
        <source>Add to favorite</source>
        <translation>Pridať k obľúbeným</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ReadPage.qml" line="49"/>
        <location filename="../qml/eRBook/symbian/ReadPage.qml" line="55"/>
        <source>Set as unread</source>
        <translation>Neprečítaný</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ReadPage.qml" line="51"/>
        <location filename="../qml/eRBook/symbian/ReadPage.qml" line="57"/>
        <source>Set as read</source>
        <translation>Prečítaný</translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/ReadPage.qml" line="90"/>
        <location filename="../qml/eRBook/symbian/ReadPage.qml" line="98"/>
        <source>Open in browser</source>
        <translation>Otvoriť v prehliadači</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="26"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="29"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="65"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="80"/>
        <source>Refresh cache</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="95"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="117"/>
        <source>Cache restored</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="124"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="144"/>
        <source>User info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="134"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="155"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="154"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="177"/>
        <source>Sign Out</source>
        <extracomment>LogOut button</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="167"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="191"/>
        <source>Sync settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="177"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="202"/>
        <source>Auto sync &lt;font size=&quot;2&quot;&gt;(startup, refresh cache)&lt;/font&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="199"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="226"/>
        <source>Theme settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="209"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="237"/>
        <source>Night mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="229"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="259"/>
        <source>Article settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="240"/>
        <source>Font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="254"/>
        <source>Small</source>
        <extracomment>Small</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="260"/>
        <source>Medium</source>
        <extracomment>Medium</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="266"/>
        <source>Large</source>
        <extracomment>Large</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="278"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="307"/>
        <source>Sorting settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="290"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="318"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="324"/>
        <source>Order by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="304"/>
        <source>Title</source>
        <extracomment>Small</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="312"/>
        <source>Favorite</source>
        <extracomment>Medium</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="320"/>
        <source>Readed</source>
        <extracomment>Large</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="337"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="358"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="364"/>
        <source>Sort order</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="351"/>
        <source>Ascending</source>
        <extracomment>Small</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="359"/>
        <source>Descending</source>
        <extracomment>Medium</extracomment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="373"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="392"/>
        <source>Debugging</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/harmattan/SettingsPage.qml" line="383"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="403"/>
        <source>Show server side errors</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="270"/>
        <location filename="../qml/eRBook/symbian/SettingsPage.qml" line="276"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShareUI</name>
    <message>
        <location filename="../src/shareui.cpp" line="18"/>
        <source>shared with #eRBook</source>
        <extracomment>Shared with #Butaca</extracomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Splash</name>
    <message>
        <location filename="../qml/eRBook/harmattan/Splash.qml" line="33"/>
        <source>eRBook %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/symbian/Splash.qml" line="35"/>
        <source>eRBook</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../qml/eRBook/harmattan/main.qml" line="33"/>
        <location filename="../qml/eRBook/symbian/main.qml" line="39"/>
        <source>Syncing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qml/eRBook/symbian/main.qml" line="26"/>
        <source>eRBook</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
